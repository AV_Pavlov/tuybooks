\begin{tutorial}{Rhomboid tiles}

Represent the floor of the room as a rectangle on a plane lined by two families of parallel straight lines forming rhombuses as required. Consider all the intersection points of these lines that lie inside the rectangle except on its upper and right side. Denote their number by $N$. Each of them is touched from above by a part of a tile containing the \emph{same} nonempty region of the lower right quarter. So the minimum required number of tiles is at least~$N$. 

Consider first the case when the dimensions of the room are multiples of the halves of the tile diagonals: $L=n\frac{a}{2}$, $W=m\frac{b}{2}$, $n,m$ integral. In this case, one can readily see that those parts not containing any region of the lower right tile quarter can be covered from parts left from opposite walls, irrespective of whether $n$ and $m$ are even or odd. Therefore in this case no additional tiles are necessary and the minimum required number of tiles is exactly~$N$.

\begin{tabular}{ccc}
\begin{tikzpicture}
%even even
\pgfkeys{/pgf/number format/int detect}
\foreach \y in {0,1} {
	\foreach \x in {2,...,5} {
		\filldraw[color=gray] (-5+\x, 6+2*\y) -- (-5+\x, 7+2*\y) -- (-4.5+\x, 7+2*\y) -- cycle;
		\draw (-4.9+\x, 6.35+2*\y) node[anchor=south] 
			{$\scriptstyle\pgfmathparse{\x+8*\y-1}\pgfmathprintnumber{\pgfmathresult}$};
		\filldraw (-5+\x, 6+2*\y) circle(0.03);  
	}
}
\foreach \y in {0,1} {
	\foreach \x in {2,...,5} {
		\filldraw[color=gray] (-4.5+\x, 6+2*\y+1) -- (-4.5+\x, 7+2*\y+1) -- (-4.+\x, 7+2*\y+1) -- cycle;
		\draw (-4.4+\x, 6.35+2*\y+1) node[anchor=south] 
			{$\scriptstyle\pgfmathparse{3+\x+8*\y}\pgfmathprintnumber{\pgfmathresult}$};
		\filldraw (-4.5+\x, 6+2*\y+1) circle(0.03);  
	}
}

\clip(-3, 6) rectangle (1,10);
\draw (-3, 6) rectangle (1,10);
\foreach \x in {1,...,10}{
	\draw (\x, 0) -- (\x-10,20);
	\draw (\x-10, 0) -- (\x,20);
} 
\end{tikzpicture}
&
\begin{tikzpicture}
%even odd
\pgfkeys{/pgf/number format/int detect}
\foreach \y in {0,1} {
	\foreach \x in {2,...,5} {
		\filldraw[color=gray] (-5+\x, 6+2*\y) -- (-5+\x, 7+2*\y) -- (-4.5+\x, 7+2*\y) -- cycle;
		\draw (-4.9+\x, 6.35+2*\y) node[anchor=south] 
			{$\scriptstyle\pgfmathparse{\x+7*\y-1}\pgfmathprintnumber{\pgfmathresult}$};
		\filldraw (-5+\x, 6+2*\y) circle(0.03);  
	}
}
\foreach \y in {0,1} {
	\foreach \x in {2,...,4} {
		\filldraw[color=gray] (-4.5+\x, 6+2*\y+1) -- (-4.5+\x, 7+2*\y+1) -- (-4.+\x, 7+2*\y+1) -- cycle;
		\draw (-4.4+\x, 6.35+2*\y+1) node[anchor=south] 
			{$\scriptstyle\pgfmathparse{\x+7*\y+3}\pgfmathprintnumber{\pgfmathresult}$};
		\filldraw (-4.5+\x, 6+2*\y+1) circle(0.03);  
	}
}

\clip(-3, 6) rectangle (0.5,10);
\draw (-3, 6) rectangle (0.5,10);
\foreach \x in {1,...,10}{
	\draw (\x, 0) -- (\x-10,20);
	\draw (\x-10, 0) -- (\x,20);
} 
\end{tikzpicture}
&
\begin{tikzpicture}
%odd odd
\foreach \y in {0,1,2} {
	\foreach \x in {2,...,4} {
		\filldraw[color=gray] (-5+\x, 6+2*\y) -- (-5+\x, 7+2*\y) -- (-4.5+\x, 7+2*\y) -- cycle;
		\draw (-4.9+\x, 6.35+2*\y) node[anchor=south] 
			{$\scriptstyle\pgfmathparse{\x+5*\y-1}\pgfmathprintnumber{\pgfmathresult}$};
		\filldraw (-5+\x, 6+2*\y) circle(0.03);  
	}
}
\foreach \y in {0,1} {
	\foreach \x in {2,...,3} {
		\filldraw[color=gray] (-4.5+\x, 6+2*\y+1) -- (-4.5+\x, 7+2*\y+1) -- (-4.+\x, 7+2*\y+1) -- cycle;
		\draw (-4.4+\x, 6.35+2*\y+1) node[anchor=south] 
			{$\scriptstyle\pgfmathparse{\x+7*\y+2}\pgfmathprintnumber{\pgfmathresult}$};
		\filldraw (-4.5+\x, 6+2*\y+1) circle(0.03);  
	}
}

\clip(-3, 6) rectangle (-0.5,11);
\draw (-3, 6) rectangle (-0.5,11);
\foreach \x in {1,...,10}{
	\draw (\x, 0) -- (\x-10,20);
	\draw (\x-10, 0) -- (\x,20);
} 
\end{tikzpicture}
\\
$n=8, m=4,$			& $n=7, m=4$	& $n=5, m=5$	
\\
$N = 16$	& $N=14$	& $N=13$	
\end{tabular}

In the general case, when $L/a$ and $W/b$ are not integer or half-integer, we can increase the room dimensions so that these numbers reach the nearest integer or half-integer. This will reduce the situation to the above special case, and won't bring any new intersection points inside the rectangle, so the number $N$ will stay the same.

Now $N$ can be counted by examining the cases depending on $n$ and $m$ being odd or even, or by looping, however these are equivalent to the formula 
$$ N = \left\lceil\frac{nm}{2}\right\rceil,$$
where $\lceil x\rceil$ denotes the ceiling of $x$.

We can also satisfy the requirements and perhaps get a smaller number after rotating the rectangle 90 degrees. Therefore the final answer is 
$$ N = \min\left(\left\lceil\frac{\lceil\frac{2L}{a}\rceil \lceil\frac{2W}{b}\rceil}{2}\right\rceil, \left\lceil\frac{\lceil\frac{2L}{b}\rceil \lceil\frac{2W}{a}\rceil}{2}\right\rceil \right).$$

\end{tutorial}
